#!/bin/sh

headerfile='../app/views/layouts/_header.html.erb'
footerfile='../app/views/layouts/_footer.html.erb'
metadatafile='../app/views/layouts/_metadata.html.erb'
addonsfile='../app/views/layouts/_addons.html.erb'
applicationfile='../app/views/layouts/application.html.erb'

headercontent=''
footercontent=''
addonscontent=''
metadatacontent='<title>Title</title>\n
\n
<!-- site metadata -->\n
<meta name="keywords" content="">\n
<meta name="description" content="">\n
<meta name="author" content="">\n
<link rel="shortcut icon" href="">\n
<link rel="image_src" href="">\n
<link rel="canonical" href=""/>\n
\n
<!-- Open Graph metadata -->\n
<meta property="og:image" content=""/>\n
<meta property="og:url" content=""/>\n
<meta property="og:title" content=""/>\n
<meta property="og:description" content=""/>'

function CreateFile
{
  local file="$1" content="$2"
 	if [[ ! -f $file ]]; then
 	echo  Creating the new partial...
	echo $content > $file
	else
	read -r -p "This partial already exists! Do you want to overwrite it? Y/N: " -n 1 response
	echo
	if [[ $response =~ [yY](es)* ]]; then
		echo Overwiting the existing partial...
		echo $content > $file
	else
		echo Existing partial retained...
	fi
	fi
}

echo '## GENERATING PARTIALS ##'

echo '* header partial'
CreateFile $headerfile "$headercontent"
echo '* footer partial'
CreateFile $footerfile "$footercontent"
echo '* addons partial'
CreateFile $addonsfile "$addonscontent"
echo '* metadata partial'
CreateFile $metadatafile "$metadatacontent"

echo '## UPDATING THE APPLICATION LAYOUT ##'

function insertAfter {
   local file="$1" line="$2" newText="$3"
   sed -i -e "/^$line$/a"$'\\\n'"$newText"$'\n' "$file"
}

function AddToApplicationFile {
	local reference="$1" result="$2"
	insertAfter $applicationfile \
	"$reference" \
	"$result"
}

addonsreference="\  <%= csrf_meta_tags %>"
addonsresult="\  <%= render 'layouts/addons' %>"
AddToApplicationFile "$addonsreference" "$addonsresult"

metadatareference="<head>"
metadataresult="\  <%= render 'layouts/metadata' %>"
AddToApplicationFile "$metadatareference" "$metadataresult"

headerreference="<body>"
headerresult="\  <%= render 'layouts/header' %>"
AddToApplicationFile "$headerreference" "$headerresult"

footerreference="<%= yield %>"
footerresult="\  <%= render 'layouts/footer' %>"
AddToApplicationFile "$footerreference" "$footerresult"

echo '## CLEANING UP ##'

sed -i -e '/<title>/d' $applicationfile
sed -i -e '/^$/d' $applicationfile
sed -i -e '$!N; /^\(.*\)\n\1$/!P; D' $applicationfile

function RemoveCopies {
	local file="$1"
	if [[ -f $file-e ]]; then
		rm $file-e
	fi
}

RemoveCopies $metadata
RemoveCopies $applicationfile
