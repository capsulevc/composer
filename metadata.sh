#!/bin/sh

metadatafile='../app/views/layouts/_metadata.html.erb'
applicationfile='../app/views/layouts/application.html.erb'
metadata='<title>Title</title>\n
\n
<!-- site metadata -->\n
<meta name="keywords" content="">\n
<meta name="description" content="">\n
<meta name="author" content="">\n
<link rel="shortcut icon" href="">\n
<link rel="image_src" href="">\n
<link rel="canonical" href=""/>\n
\n
<!-- Open Graph metadata -->\n
<meta property="og:image" content=""/>\n
<meta property="og:url" content=""/>\n
<meta property="og:title" content=""/>\n
<meta property="og:description" content=""/>'

echo '## METADATA ##'
if [[ ! -f $metadatafile ]]; then
 	echo  Creating the new metadata partial...
	echo $metadata > $metadatafile
	else
	read -r -p "It looks like the metadata partial already exists! Shall I overwrite it? Y/N: " -n 1 response
	echo
	if [[ $response =~ [yY](es)* ]]; then
		echo Overwiting the existing metadata partial...
		echo $metadata > $metadatafile
	else
		echo Existing metadata partial retained...
	fi
fi

if grep -q "<%= render 'layouts/metadata' %>" $applicationfile; then
	echo 'The partial is already installed in the application layout file...'
	else
	echo 'Installing the partial into the application layout file...'
	function insertAfter
	{
   local file="$1" line="$2" newText="$3"
   sed -i -e "/^$line$/a"$'\\\n'"$newText"$'\n' "$file"
	}
	insertAfter $applicationfile \
	"<head>" \
	"\  <%= render 'layouts/metatags' %>"
fi

