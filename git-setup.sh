#!/bin/sh

echo '## GIT SETUP  ##'
# gem install bitbucket-cli

echo '* initialising git'
git init
echo '* first commit'
git add .
git commit -am 'first commit'

read -r -p "Add to BitBucket y/n: " -n 1 response
	if [[ $response =~ [yY](es)* ]]; then
		echo '* adding to BitBucket'
		# echo -n -e 'What is your BitBucket username (email)?\n > '
		# read login
		# echo -n -e 'What is your BitBucket password?\n > '
		# stty -echo
		# read password
		# stty echo
		echo -n -e '\n What is this repo called?\n > '
		read repository
		"curl --user hi@ben.gy:genius https://api.bitbucket.org/1.0/repositories/ --data name=$repository --data owner=capsulvc --data is_private=true"
		git remote add origin https://git@bitbucket.org:capsulevc/$repository.git
		git push -u origin --all
		git push -u origin --tags
	else
	echo '* all done!'
fi
